#!/usr/bin/env python3
import rospy
import cv2 as cv
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np

image_pub = rospy.Publisher('/zed/zed_node/rgb/image_rect_color', Image, queue_size=1)
depth_pub = rospy.Publisher('/zed/zed_node/depth/depth_registered', Image, queue_size=1)


rospy.init_node('zed_mock')
rospy.loginfo('started zed mocking')

cap = cv.VideoCapture(0)
bridge = CvBridge()

ite = 0

while True:
    ret, frame = cap.read()
    frame_id = str(ite)
    image_msg = bridge.cv2_to_imgmsg(frame, encoding="bgr8")
    image_msg.header.frame_id = frame_id
    image_msg.header.stamp = rospy.Time.now()
    image_pub.publish(image_msg)
    depth_msg = bridge.cv2_to_imgmsg(np.zeros_like(frame), encoding="bgr8")
    depth_msg.header.frame_id = frame_id
    depth_msg.header.stamp = image_msg.header.stamp
    depth_pub.publish(depth_msg)
    ite += 1
