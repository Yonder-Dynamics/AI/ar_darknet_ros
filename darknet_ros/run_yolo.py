#!/usr/bin/env python3
import rospy
import serial, time
from cv_bridge import CvBridge, CvBridgeError
import message_filters
import cv2 as cv
import actionlib
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseArray
from functools import reduce
from darknet_ros_msgs.msg import BoundingBox, BoundingBoxes, CheckForObjectsAction, CheckForObjectsGoal
import datetime
import matplotlib.pyplot as plt
from threading import Lock
import random
import std_msgs.msg

thresh = .3
tile_scale = 1

class ImageTile:
    ### x, y are the coordinates of the top left corner of tile in the original image
    def __init__(self, image, x, y, tile_scale):
        self.image = image;
        self.x = x;
        self.y = y;
        self.tile_scale = tile_scale;

###
# args:
# image - image
# width, height - how many tiles to split the image into along each axis
# overlap - overlap of tiles. Should be the maximum dimension we expect a tag to
#   take in the image
# returns:
#   [ImageTile] - the tiles
###
def tile_image(image, width, height, overlap=40):
    tile_height = int(image.shape[0] / height + overlap / 2)
    tile_width = int(image.shape[1] / width + overlap / 2)
    y_stride = tile_height - overlap
    x_stride = tile_width - overlap
    tiles = []
    for y in range(height):
        for x in range(width):
            sub_img = image[y*y_stride:y*y_stride+tile_height,
                            x*x_stride:x*x_stride+tile_width,:]
            sub_img = cv.resize(sub_img, (tile_scale * tile_width, tile_scale * tile_height))
            tiles.append(ImageTile(sub_img, x*x_stride, y*y_stride, tile_scale))
    tiles.append(ImageTile(image, 0, 0, 1))
    return tiles

def yolo_detect(image):
    yolo_client = actionlib.SimpleActionClient('/darknet_ros/check_for_objects', CheckForObjectsAction)
    yolo_client.wait_for_server()
    goal = CheckForObjectsGoal(0, bridge.cv2_to_imgmsg(image, "rgb8"))
    yolo_client.send_goal(goal)
    print(yolo_client.wait_for_result())
    boxes = yolo_client.get_result().bounding_boxes
    return boxes

def valid_bbox(bbox):
    return (bbox.id == "tag" and bbox.probability > thresh)

def cull_redundant(acc, bbox):
    for existing in acc:
        if IOU(existing, bbox) > .2:
            return acc
    return acc + [bbox]

def IOU(boxA, boxB):
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)
    # return the intersection over union value
    return iou

# timestamp -> imageprocessor instance
imgmap = {}
def bbox_received(message):
    # process image tile result
    if message.image_header.stamp in imgmap:
        (processor, tile) = imgmap[message.image_header.stamp]
        with processor.lock:
            processor.boxes.append((message, tile))
            processor.check_done()
        del imgmap[message.image_header.stamp]
    else:
        # I modified darknet_ros to run detection on a dummy image if no images
        # are in the queue, will fix later (tm), for now we do nothing
        pass

def NMS(bboxes):
    keeplist = [True for box in bboxes]
    for i, box in enumerate(bboxes[:-1]):
        for j, other in enumerate(bboxes[i+1:]):
            interArea = max(0, min(box.xmax, other.xmax) - max(box.xmin, other.xmin)) * \
                        max(0, min(box.ymax, other.ymax) - max(box.ymin, other.ymin))
            boxArea = (box.xmax - box.xmin) * (box.ymax - box.ymin)
            otherArea = (other.xmax - other.xmin) * (other.ymax - other.ymin)
            IOU = interArea / float(boxArea + otherArea - interArea)
            if IOU > .05:
                keeplist[min((box.probability, i), (other.probability, j))[1]] = False
    res = []
    print(keeplist)
    for keep, box in zip(keeplist, bboxes):
        if keep:
            res.append(box)
    return res

class ImageProcessor:
    def __init__(self, image, header):
        tiles = tile_image(image, 3, 2)[:-1]
        self.image = image
        self.numtiles = len(tiles)
        self.lock = Lock()
        self.boxes = []
        self.header = header
        for i, tile in enumerate(tiles):
            imagemsg = bridge.cv2_to_imgmsg(tile.image, "rgb8")
            imagemsg.header.stamp = rospy.Time.now()
            imgmap[imagemsg.header.stamp] = (self, tile)
            yolo_process_pub.publish(imagemsg)

    def check_done(self):
        if len(self.boxes) == self.numtiles:
            self.done()

    # When all tiles have been received
    def done(self):
        ready()
        abs_boxes = [] # Boxes in relation to absolute
        for (boxes, tile) in self.boxes:
            for box in boxes.bounding_boxes:
                box.xmin = box.xmin + tile.x
                box.xmax = box.xmax + tile.x
                box.ymin = box.ymin + tile.y
                box.ymax = box.ymax + tile.y
            abs_boxes.extend(boxes.bounding_boxes)
        abs_boxes = NMS(abs_boxes)
        for bbox in abs_boxes:
            self.image = cv.rectangle(self.image, (int(bbox.xmin), int(bbox.ymin)),
                        (int(bbox.xmax), int(bbox.ymax)), (255, 0, 0), 2)
        print(abs_boxes)
        img_msg = bridge.cv2_to_imgmsg(self.image, encoding="bgr8")
        ar_image_pub.publish(img_msg)
        ar_bboxes_pub.publish(std_msgs.msg.Header(), self.header, abs_boxes)

def ready():
    while yolo_process_pub.get_num_connections() == 0:
        pass
    try:
        message = rospy.wait_for_message('/zed/zed_node/rgb/image_rect_color', Image)
        ImageProcessor(bridge.imgmsg_to_cv2(message, "bgr8"), message.header)
    except CvBridgeError as e:
        print(e)

rospy.init_node('ar_detector')

bridge = CvBridge()
# ar_pose_pub = rospy.Publisher('ar_pose', PoseArray, queue_size=1)
ar_image_pub = rospy.Publisher('/armodel/bboximg', Image, queue_size=1)
ar_bboxes_pub = rospy.Publisher('/armodel/bboxes', BoundingBoxes, queue_size=1)

yolo_process_pub = rospy.Publisher('/camera/rgb/image_raw', Image, queue_size=10)

bbox_sub = rospy.Subscriber('/darknet_ros/bounding_boxes', BoundingBoxes, bbox_received)

ready()

rospy.loginfo("started ar tag detector")
rospy.spin()
