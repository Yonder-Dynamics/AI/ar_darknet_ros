# AR Tag Detection
 
First clone this repo into your catkin workspace source folder.
Make sure that CUDA is installed on the device, and that the zed camera is plugged in.
If you're working on the Jetson, these things are probably already set up.
Make sure to source the setup file with `source ~/catkin_ws/setup.zsh` (or equivalent file for the environment).
To run the detection, simply do `roslaunch darknet_ros yolo_v3.launch`.
 
We are using YOLOV3 to detect AR Tags, you can read up on it here: https://pjreddie.com/darknet/yolo/.
